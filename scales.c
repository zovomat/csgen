/*
 *@file chords.c
 *@brief
 *
 *@author
 *@version
 *@since
 *
 *@copyright
 *@copyright
 */

/* libc include for the printf function */
#include <stdio.h> 

/* libc include for the memory functions */
#include <stdlib.h>

/* libc include for the string functions */
#include <string.h>

/* libc include for the char functions */
#include <ctype.h>

#include "header.h"

#define N 5
struct scale {
	char name[20];
	char code[20];
	char intervals[20];
	};
struct scale scales[N];

int parseScales();
int printScales(int nScales);
int generateScale(int notation);
/*
 *@brief
 *
 *@param
 *@return
 */
extern int mainScale(int notation){
	int run=1;
	int nScales=parseScales();
	char op;
	printf("\n-----Welcome to the Scale Generator!-----\n  %d scales are available!", nScales);
	while(run){
		printf("\n  |Enter:\n  |[H] for a list of available scales\n  |[S] to start generating\n  |[Q] to return to main menu\n  |-->");
		op=getchar();
		while(getchar()!='\n');
		if(op=='S'||op=='s'){
			generateScale(notation);
			}else if(op=='H'||op=='h'){
			printf("\n These are the currently available scales:\n");
			printScales(nScales);
			}else if(op=='Q'||op=='q'){
			run=0;
			}else{
				printf("\nInvalid operation character\n");
			}
		}

	return run;
}

int parseScales(){
		FILE* sf;
		int i=0;
		sf=fopen("scales.txt","r");
		rewind(sf);

		while(	fscanf(sf,"%s", scales[i].name)!=EOF){
			
			fscanf(sf, "%s %s",scales[i].code, scales[i].intervals);
			i++;
			
		/*	if (fscanf(sf,"<%s> <%s> <%s>", name, code, sequence)){
				printf("error parsing lsine %d \n", i);
			}else{
				printf("line %d: %s, %s, %s \n", i, name, code, sequence);
			}
		*/	
		}
		
		fclose(sf);
		return i;
}

int printScales(int nScales){
	int j;
		
		for(j=0; j<nScales; j++){		
			printf("|%15s|%2s|%-12s| \n",scales[j].name, scales[j].code, scales[j].intervals );
		}
}

int generateScale(int notation){
	char key[4];
	int keySelected=0;
	while(!keySelected){
		printf("Please insert Key:\n ");
		scanf("%s",key);
		while(getchar()!='\n');
		keySelected=1;
		printf("#######DEBUG: completare con ricerca e validazione chiave ");
	}
}

