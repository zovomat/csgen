/*
 *@file chords.c
 *@brief
 *
 *@author
 *@version
 *@since
 *
 *@copyright
 *@copyright
 */

/* libc include for the printf function */
#include <stdio.h> 

/* libc include for the memory functions */
#include <stdlib.h>

/* libc include for the string functions */
#include <string.h>

/* libc include for the char functions */
#include <ctype.h>

#include "header.h"

#define N 1
struct chord {
	char name[15];
	char code;
	char intervals[];
	};
struct chord chord[N];


/*
 *@brief
 *
 *@param
 *@return
 */
extern int generateChord(int notation){
	int run=1;
	char op;
	printf("\n-----Welcome to the Chord Generator!-----\n");
	while(run){
		printf("\n  |Enter:\n  |[H] for a list of available chords\n  |[S] to start generating\n  |[Q] to return to main menu\n  |-->");
		op=getchar();
		while(getchar()!='\n');
		if(op=='S'||op=='s'){
			printf("\n###########  DEBUG   ########### here goes the input sequence\n");
			}else if(op=='H'||op=='h'){
			printf("\n These are the currently available chords:\n");
			printf("\n###########  DEBUG   ########### here goes the help sequence\n");
			}else if(op=='Q'||op=='q'){
			run=0;
			}else{
				printf("\nInvalid operation character\n");
			}
		}

	return run;
}

